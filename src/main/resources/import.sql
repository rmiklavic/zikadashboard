BEGIN;

CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS postgis_topology;
CREATE EXTENSION IF NOT EXISTS fuzzystrmatch;
CREATE EXTENSION IF NOT EXISTS postgis_tiger_geocoder;
CREATE EXTENSION IF NOT EXISTS unaccent;

COPY report(report_date,location,location_type,data_field,data_field_code,time_period,time_period_type,val,unit,location_geometry) FROM '/tmp/Brazil_Zika-2016-04-02.csv' DELIMITER ',' CSV HEADER;

COMMIT;
